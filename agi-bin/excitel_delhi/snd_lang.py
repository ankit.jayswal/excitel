#!/usr/bin/python3


####################################################
##Excitel _Delhi                                   ##
####################################################

excitel_de_welcome			= 'excitel_delhi/Welcome'
excitel_de_new_user_menu_de		= 'excitel_delhi/New_User_Menu_de'
excitel_de_exst_user_menu_de	        = 'excitel_delhi/Exisitng_User_Menu'
excitel_de_enter_rmn_de			= 'excitel_delhi/enterRMN_deN'
excitel_de_in_rmn_de			= 'excitel_delhi/invalidRMN_deN'
excitel_invalid_input			= 'excitel_delhi/Invalid_input'
no_selection				= 'excitel_delhi/noInput'
excitel_press_1				= 'excitel_delhi/Press_1'
press1					= 'excitel_delhi/Press_1'

press1_hin				= 'excitel_delhi/Hindi-Speak_to_agent_1'
excitel_de_rmn_go                       = 'excitel_delhi/invalidRMN_Go'            
excitel_queue_tr                        = 'excitel_delhi/TQannouncement'

###################################################
# Working Hours                                  ##
###################################################

excitel_sales_hours				=	'excitel_delhi/Sales_Working_Hours'
excitel_nodal_hours 	        =	'excitel_delhi/Nodal_Working_Hours'
excitel_support_hour	        =	'exciteldelhi/Support_Working_Hours'


##################################################
## Ticket Sound Files                         ####
##################################################
salessupQ = 'ticket_sounds/en/salesnsupport'
noticketf = 'ticket_sounds/en/notickets'

S200 = 'Downtime_Annoucements/200'
S201 = 'Downtime_Annoucements/201'
S202 = 'Downtime_Annoucements/202'
S203 = 'Downtime_Annoucements/203'
S204 = 'Downtime_Annoucements/204'
S205 = 'Downtime_Annoucements/205'
S206 = 'Downtime_Annoucements/206'
S207 = 'Downtime_Annoucements/207'
S208 = 'Downtime_Annoucements/208'

S210 = 'Downtime_Annoucements/210'
S211 = 'Downtime_Annoucements/211'
S212 = 'Downtime_Annoucements/212'
S213 = 'Downtime_Annoucements/213'
S214 = 'Downtime_Annoucements/214'
S215 = 'Downtime_Annoucements/215'
S216 = 'Downtime_Annoucements/216'
S217 = 'Downtime_Annoucements/217'
S218 = 'Downtime_Annoucements/218'

S300 = 'Downtime_Annoucements/300'
S301 = 'Downtime_Annoucements/301'
S302 = 'Downtime_Annoucements/302'
S303 = 'Downtime_Annoucements/303'
S304 = 'Downtime_Annoucements/304'
S305 = 'Downtime_Annoucements/305'
S306 = 'Downtime_Annoucements/306'
S307 = 'Downtime_Annoucements/307'
S308 = 'Downtime_Annoucements/308'
