#!/usr/bin/python3

############################
#   Exceitel CX_01 Script  #
#   Code : CJ              #
#                          #
############################


import pandas as pd
from asterisk.agi import *

agi = AGI()

agi.answer()
agi.verbose("Checking VIP Caller...")

CallerDNID = sys.argv[1][-10:]
did_num = sys.argv[2][-8:]

agi.verbose("CallerID is : %s" %CallerDNID)
agi.verbose("DIDnum is : %s" %did_num)

#filepath = "/var/lib/asterisk/agi-bin/excitel_delhi"+"/data/AvayaVIP.csv"
filepath = "/var/lib/asterisk/agi-bin/excitel_delhi/data/vip_"+str(did_num)+".csv"

agi.verbose("File is : %s" %filepath)

try:
    data=pd.read_csv(filepath)
    if float(CallerDNID) in list(data['Mobile number']):
        vip = True
    else:
        vip = False

except Exception as e:
    vip =False

agi.set_variable('vip',vip)
