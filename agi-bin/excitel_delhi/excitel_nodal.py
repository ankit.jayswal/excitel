#!/usr/bin/python3

############################
#   Exceitel CX_01 Script  #
#   Code : CJ              #
#                          #
############################


import sys
import json
import logging
import log_config
import requests
import snd_lang
import ex_work_hours_dept

from snd_lang import *
from asterisk.agi import *
from Ex_functions import *
from ex_work_hours_dept import *
from get_queue_agents import get_queue_agents

agi = AGI()


agi.verbose("Excitel Delhi Nodal IVR Application Started - CX_01")
logging.warning('Excitel Delhi Nodal IVR Application Started - CX_01')
agi.answer()

CallerDNID = sys.argv[1]

DEFAULT_TIMEOUT=10000

logging.warning('Streaming Welcome Menu message')
agi.stream_file(excitel_de_welcome,escape_digits='*,#')


def excitelNodalworkhours():
    agi.verbose('Checking Nodal Working Hours')
    logging.warning('Checking Nodal Working Hours')
    wrkhrsResult = excitelSupportworkinghours()
    agi.verbose('Nodal Working Hours is %s' % wrkhrsResult)
    logging.warning('Nodal Working Hours is %s' % wrkhrsResult)
    if wrkhrsResult == 'Working Hours':
        agi.verbose('It is Working Hours for Nodal')
        logging.warning('It is Working Hours for Nodal')
        agi.verbose('checking available agent status on workers in Nodal Queue')
        logging.warning('Checking available agent status on workers in Nodal Queue')
        aapl_queue_status = get_queue_agents('delhinodal')

        agi.verbose('Agent Status on worker%s' % aapl_queue_status)
        if aapl_queue_status:
            agi.goto_on_exit(context='excitel_appl_fm',extension='s',priority='')
        else:
            agi.goto_on_exit(context='from-internal',extension='8007',priority='')

    elif wrkhrsResult == 'Non Working Hours':
        agi.verbose('It is Non Working Hours for Nodal')
        logging.warning('It is Non Working Hours for Nodal')
        agi.stream_file(excitel_support_hour)
        agi.hangup()
    elif wrkhrsResult == 'Non Working Day':
        agi.verbose('It is Non Working Hours for Nodal')
        logging.warning('It is Non Working Hours for Nodal')
        agi.stream_file(excitel_support_hour)
        agi.hangup()


    
nodalWH = excitelNodalworkhours()





