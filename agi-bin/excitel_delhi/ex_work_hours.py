#!/usr/bin/python3

#import pandas as pd
from datetime import datetime



def excitelAppaleteworkinghours():

    working_days = ['Monday','Tuesday','Wednesday','Thursday','Friday']
    non_working_days = ['Saturday','Sunday']
    start_hour = int('10')
    start_minutes = int('00')
    end_hour = int('15')
    end_minutes = int('00')
    now = datetime.now()

    current_time = now.strftime("%H:%M:%S")
    current_hour = int(now.strftime("%H"))
    current_minute = int(now.strftime("%M"))

    day_name = now.strftime("%A")


    if current_hour >= start_hour and current_hour < end_hour and day_name in working_days:
        return "Working Hours"
    elif current_hour >= end_hour and day_name in working_days:
        return "Non Working Hours"
    else:
        return "Non Working Hours"
        
    


def excitelPartnerworkinghours():

    working_days = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday']
    start_hour = int('08')
    start_minutes = int('00')
    end_hour = int('18')
    end_minutes = int('00')
    now = datetime.now()

    current_time = now.strftime("%H:%M:%S")
    current_hour = int(now.strftime("%H"))
    current_minute = int(now.strftime("%M"))

    day_name = now.strftime("%A")


    if current_hour >= start_hour and current_hour<end_hour and current_hour != end_hour and day_name in working_days:
        return "Working Hours"
    elif current_hour >= end_hour and day_name in working_days:
        return "Working Hours"
    else:
        return "Working Hours"



def excitelSupportworkinghours():

    working_days = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday']
    start_hour = int('10')
    start_minutes = int('00')
    end_hour = int('15')
    end_minutes = int('00')
    now = datetime.now()

    current_time = now.strftime("%H:%M:%S")
    current_hour = int(now.strftime("%H"))
    current_minute = int(now.strftime("%M"))

    day_name = now.strftime("%A")


    if current_hour >= start_hour and current_hour < end_hour and day_name in working_days:
        return "Working Hours"
    elif current_hour >= end_hour and day_name in working_days:
        return "Non Working Hours"
    else:
        return "Non Working Hours"


def excitelSalesworkinghours():
    working_days = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday']
    start_hour = int('09')
    start_minutes = int('00')
    end_hour = int('21')
    end_minutes = int('00')
    now = datetime.now()

    current_time = now.strftime("%H:%M:%S")
    current_hour = int(now.strftime("%H"))
    current_minute = int(now.strftime("%M"))

    day_name = now.strftime("%A")


    if current_hour >= start_hour and current_hour<end_hour and current_hour != end_hour and day_name in working_days:
        return "Working Hours"

    elif current_hour >= end_hour and day_name in working_days:
        return "Non Working Hours"

    else:
        return "Non Working Hours"
