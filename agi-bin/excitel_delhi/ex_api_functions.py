#!/usr/bin/python3

############################
#   Exceitel CX_01 Script  #
#   Code : CJ              #
#                          #
############################


import sys
import json
import logging
import log_config
import requests


def CheckSubscriberStatus(subsNumber,tenant):
    
    No_User = [0]
    User_Other = [1]
    User_WO_T = [2]
    Partner_Response = [100]
    LocalTickets_ErrorCodes = [200,201,202,203,204,205,206,207,208]
    LocalTicketsOth_ErrorCode = [210,211,212,213,214,215,216,217,218]
    GlobalTickets_ErrorCode = [300,301,302,303,304,305,306,307,308]

    
    logging.warning('Checking Caller Phone Number for: %s' % subsNumber)
    #URL = 'https://staging-del-desk.excitel.in/api/tixets/public/index.php/externalApi/subscriber/selectIVRFlowByPhoneOwner'
    #URL = 'https://del-desk.excitel.in/api/tixets/public/index.php/externalApi/subscriber/selectIVRFlowByPhoneOwner'
    #PARAMS  = {"apiName":"avaya","apiKey":"$2a$12$Q76SH0pq0ypMQHEncs1m9uKoTa.xDMgCzRZcsxR/lJIaFk1I6se96","phone":subsNumber}
    URL = 'https://excitel-contactcenter-prerelease.azurewebsites.net/api/v1.0/excitel/Subscriber/IVRFlowByPhoneNumber?tenantName='+tenant+'&phoneNumber='+subsNumber+'&keyName=engagely&key=7a19df9fafbc41e6bee02a75c22b115e'
    logging.warning('Caller check API URL %s' % URL)
    #logging.warning('Caller Check API Parameters %s' % PARAMS)
    #response = requests.post(URL, data = PARAMS)
    response = requests.get(URL)
    if response.status_code == 200:

        json_data = response.json()

        getcode = json_data['result']['statusCode']
    elif response.status_code ==400:
        getcode = 0
    # getcode = 310
    logging.warning('Caller Check Time, API Response Code %s' % getcode)

    if getcode in No_User:
        logging.warning('Error Code 0 detected : 0 - no matches found %s' % getcode)
        return getcode
    elif getcode in User_Other:
        logging.warning('Error Code 1 detected : 1 - calling number found in more than one customer %s' % getcode)
        return getcode
    elif getcode in User_WO_T:
        logging.warning('Error Code 2 Detected : 2 -  2 - calling number found in single customer and there is no Local/Global ticket for it %s' % getcode)
        return getcode
    elif getcode in Partner_Response:
        logging.warning('Error Code 100 Detected 100 - calling number found in Partner or Partner Employee %s' % getcode)
        return getcode
    elif getcode in LocalTickets_ErrorCodes:
        logging.warning('Local Ticket Found %s' % getcode)
        return getcode
    elif getcode in LocalTicketsOth_ErrorCode:
        logging.warning('Local Tickets Othere Found : %s' % getcode)
        return getcode
    elif getcode in GlobalTickets_ErrorCode:
        logging.warning('Global Tickets Found : %s' % getcode)
        return getcode
    else:
        logging.warning('Unknown Response detected')


def CheckCallerStatusAgain(CallerNumber):
    logging.warning('Checking Caller Phone Number for: %s' % subsNumber)
    #URL = 'https://staging-del-desk.excitel.in/api/tixets/public/index.php/externalApi/subscriber/selectIVRFlowByPhoneOwner'
    URL = 'https://del-desk.excitel.in/api/tixets/public/index.php/externalApi/subscriber/selectIVRFlowByPhoneOwner'
    PARAMS  = {"apiName":"avaya","apiKey":"$2a$12$Q76SH0pq0ypMQHEncs1m9uKoTa.xDMgCzRZcsxR/lJIaFk1I6se96","phone":subsNumber}
    logging.warning('Caller check API URL %s' % URL)
    logging.warning('Caller Check API Parameters %s' % PARAMS)
    response = requests.post(URL, data = PARAMS)
    json_data = response.json()

    getcode = json_data['result']['statusCode']
    logging.warning('Caller Check Time, API Response Code %s' % getcode)

    if getcode in No_User:
        logging.warning('Error Code 0 detected : 0 - no matches found %s' % getcode)
        return getcode
    elif getcode in User_Other:
        logging.warning('Error Code 1 detected : 1 - calling number found in more than one customer %s' % getcode)
        return getcode
    elif getcode in User_WO_T:
        logging.warning('Error Code 2 Detected : 2 -  2 - calling number found in single customer and there is no Local/Global ticket for it %s' % getcode)
        return getcode
    elif getcode in Partner_Response:
        logging.warning('Error Code 100 Detected 100 - calling number found in Partner or Partner Employee %s' % getcode)
        return getcode
    elif getcode in LocalTickets_ErrorCodes:
        logging.warning('Local Ticket Found %s' % getcode)
        return getcode
    elif getcode in LocalTicketsOth_ErrorCode:
        logging.warning('Local Tickets Othere Found : %s' % getcode)
        return getcode
    elif getcode in LocalTicketsOth_ErrorCode:
        logging.warning('Global Tickets Found : %s' % getcode)
        return getcode
    else:
        logging.warning('Unknown Response detected')

