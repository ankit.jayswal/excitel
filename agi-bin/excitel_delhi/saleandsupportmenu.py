
import sys
import json
import logging
import log_config
import requests


def ExistingSNSQueue(userdtmfin,agi):
    agi.verbose('Will go into Sales and Support Queue')
    logging.warning('Existing user sales and support selection')
    agi.verbose('User Pressed %s' % userdtmfin)
    logging.warning('Caller Selected  %s' % userdtmfin)
    if userdtmfin == '1':
        logging.warning('Going to Customer Support Queue')
        agi.goto_on_exit(context='excitel_customerser',extension='s',priority='')
    elif userdtmfin == '2':
        logging.warning('Going to Customer Support Queue')
        agi.goto_on_exit(context='excitel_customerser',extension='s',priority='')


def SalesSupportQ(agi):
    agi.verbose('Into Sales nd Support Queue')
    logging.warning('Sales and Support Queue Selection Menu')
    val_digit = ['1']
    inv_digit = ['3','4','5','6','7','8','9','0']
    no_digit = ['']
    
    inv_cnt = 2
    no_cnt = 2
    
    while inv_cnt > 0 and no_cnt > 0 :
        agi.verbose('Inside while')
        logging.warning('Streaming file for Sales and Support Queue')
        get_input = agi.get_data(salessupQ,DEFAULT_TIMEOUT,max_digits=1)
        agi.verbose('User Pressed %s' % get_input)

        if get_input in val_digit:
            agi.verbose('Valid digit recevied %s' % get_input)
            logging.warning('Caller selected valid selection  %s' % get_input)
            gotosnsQueues = ExistingSNSQueue(get_input,agi)
            break
        elif get_input in inv_digit:
            agi.verbose('Invalid digit recevied %s' % get_input)
            logging.warning('Caller selected invalid selection  %s' % get_input)
            inv_cnt -= 1
            agi.stream_file(invalid_selection)
        elif get_input in no_digit:
            agi.verbose('No digit recevied %s' % get_input)
            logging.warning('Caller selected no selection  %s' % get_input)
            no_cnt -= 1
            agi.stream_file(no_selection)