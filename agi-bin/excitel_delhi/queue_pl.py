#!/usr/bin/env python3

############################
#   Exceitel CX_01 Script  #
#   Code : CJ              #
#                          #
############################


import sys
import json
import logging
import log_config
import requests
import snd_lang
from snd_lang import *
from asterisk.agi import *


agi = AGI()

agi.verbose("Executing Queue Script")
logging.warning('Executing Queue Script')
agi.answer()

logging.warning('Streaming Transfer message ')
agi.stream_file(excitel_queue_tr,escape_digits='')

