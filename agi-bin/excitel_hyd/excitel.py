#!/usr/bin/python3

############################
#   Exceitel CX_01 Script  #
#   Code : CJ              #
#                          #
############################


import sys
import json
import logging
import log_config
import requests
import snd_lang
import ex_work_hours
import ex_api_functions
# import ex_prior_num
# import ex_call_freq 
from repeat_call import repeat_caller_check
from snd_lang import *
from asterisk.agi import *
from ex_api_functions import *
from ex_work_hours import *
from get_queue_agents import get_queue_agents
import mysql.connector
#from vip_cust import inexcel
# from ex_prior_num import *
# from ex_call_freq import * 

agi = AGI()

agi.verbose("Excitel Main IVR Application Started ")
logging.warning('Excitel Main IVR Application Started ')
agi.answer()

DEFAULT_TIMEOUT = 5000
CallerDNID = sys.argv[1]

agi.verbose('Calling number %s' % sys.argv[1])
# agi.verbose("call %s",sys.argv[1])

CallerDNID = sys.argv[1][-10:]
hist_did = sys.argv[2][-10:]
agi.verbose('HIST DID %s' % hist_did)

#mysql connection
mydb = mysql.connector.connect(host='192.168.202.22',user='admin',password='8FRf4T',database='asterisk')
mycursor = mydb.cursor()
mycursor.execute("SELECT * FROM tenant WHERE did_number='"+hist_did+"'")
myresult = mycursor.fetchall()
if(myresult):
    tenant_name = myresult[0][2]
else:
    tenant_name = 'delhi'

agi.verbose('TENANT NAME %s' % tenant_name)

logging.warning('Streaming Welcome Menu message ')
agi.stream_file(excitel_de_welcome,escape_digits='*,#')

logging.warning('Stripped Number %s' % CallerDNID)
agi.verbose('Stripped Number %s' % CallerDNID)

No_User = [0]
User_Other = [1]
User_WO_T = [2]
Partner_Response = [100]
LocalTickets_ErrorCodes = [200,201,202,203,204,205,206,207,208]
LocalTicketsOth_ErrorCode = [210,211,212,213,214,215,216,217,218]
GlobalTickets_ErrorCode = [300,301,302,303,304,305,306,307,308]

agi.verbose('before')

CheckSubscriber = CheckSubscriberStatus(CallerDNID,tenant_name)
# CheckSubscriber = 200
agi.verbose('after')

agi.verbose('Calling User Status %s' % CheckSubscriber)
logging.warning('Calling User Status %s' % CheckSubscriber)

def CheckCallerStatus(PhNumber,tenant):
    agi.verbose('Rechecking Again for Subscriber')
    logging.warning('Rechecking Again for Subscriber')
    
    inval_input = ['*']
    no_input = ['']
    flag=False
    no_inp_count = 2
    inval_count = 2
    # chk_sub_input = agi.get_data(excitel_de_enter_rmn_de,7000,max_digits=10)
    while no_inp_count > 0 or inval_count > 0 :
        chk_sub_input=""
        logging.warning('Streaming file to enter RMN')
        if no_inp_count ==2 and inval_count ==2:
            chk_sub_input = agi.get_data(excitel_de_enter_rmn_de,7000,max_digits=10)
            agi.verbose('inside while no_inp_count->>> %s' %no_inp_count)
        else:
            chk_sub_input = agi.get_data(excitel_de_in_rmn_de,7000,max_digits=10)
            agi.verbose('inside while inval_count->>> %s' %inval_count)
        if chk_sub_input in inval_input:
            agi.verbose('Invalid digit recevied %s' % chk_sub_input)
            logging.warning('Invalid  selection by User %s' % chk_sub_input)
            no_inp_count -=  1
            inval_count   -= 1
            # agi.stream_file(excitel_de_in_rmn_de)
        elif chk_sub_input in no_input:
            agi.verbose('No digit recevied %s' % chk_sub_input)
            logging.warning('Caller selected no selection  %s' % chk_sub_input)
            no_inp_count -=  1
            inval_count   -= 1
            # agi.stream_file(excitel_de_in_rmn_de)
        else:
            CheckStatusAgain = CheckSubscriberStatus(chk_sub_input,tenant_name)
            agi.verbose('Status on Check Again %s' % CheckStatusAgain)

            if CheckStatusAgain in Partner_Response:
                gotoPartnerMenu()
                flag=True
                break
            elif CheckStatusAgain in No_User:
                # chk_sub_input = agi.get_data(excitel_de_enter_rmn_de,7000,max_digits=10)            elif CheckSubsciberAgain in User_Other:
                # CheckStatusAgain = CheckSubscriberStatus(chk_sub_input)
                # agi.stream_file(excitel_de_in_rmn_de)
                no_inp_count -= 1
                inval_count -= 1

            elif CheckStatusAgain in User_WO_T:
                gotoRegisteredUserMenu1(chk_sub_input)
                flag=True
                break

            elif CheckStatusAgain in LocalTickets_ErrorCodes:
                agi.verbose('Local ticket found with code %s' % CheckStatusAgain)
                logging.warning('Local ticket found with code %s' % CheckStatusAgain)
                gotoLocalTicketMenu(CheckStatusAgain)
                flag=True
                break
            elif CheckStatusAgain in GlobalTickets_ErrorCode:
                #gotoLocalTicketOth(chk_sub_input)
                agi.verbose('Global ticket found with code %s' % CheckStatusAgain)
                logging.warning('Global ticket found with code %s' % CheckStatusAgain)
                gotoGlobalTicket(CheckStatusAgain)
                flag=True
                break
            elif CheckStatusAgain in LocalTicketsOth_ErrorCode:
                agi.verbose('Local ticket other found with code %s' % CheckStatusAgain)
                logging.warning('Local ticket other found with code %s' % CheckStatusAgain)
                gotoLocalTicketOth(CheckStatusAgain)
                flag=True 
                break
            else:
                agi.verbose('&&&&Hanging up the call&&&&&%s'%chk_sub_input)
                agi.verbose('&&&&Hanging up the call&&&&&%s'%CheckStatusAgain)
                agi.hangup()
    agi.verbose('outside while')
    if not flag:
        agi.verbose('Stream Invalid RMN File and Goto Registered User Menu')
        logging.warning('Stream Invalid RMN File and Goto Registered User Menu')
        agi.stream_file(excitel_de_rmn_go)
        gotoRegisteredUserMenu(CallerDNID)


def gotoNewUserSelectionMenu(user_selection,tenant):
    if user_selection == '1':
        agi.verbose('Valid User Input Menu %s' % user_selection)
        logging.warning('Recheck Again for Subscription,Prompt Caller to Enter RMN ')
        CheckCallerStatus(CallerDNID,tenant)
    elif user_selection == '2':
        getSalesWorkHrs = excitelSalesworkinghours()
        if getSalesWorkHrs == 'Working Hours':
            agi.verbose('INSIDE SALES WORKING HOURS....')
            queue_agents=get_queue_agents('Hyd_sales')
            agi.verbose('return  queue_agents%s' % queue_agents)
            logging.warning('return  queue_agents%s' % queue_agents)
            agi.verbose('Valid User Input Menu %s' % user_selection)
            if queue_agents:
                agi.goto_on_exit(context='excitel_hyd_sales',extension='s',priority='1')
            else:
                agi.goto_on_exit(context='from-internal',extension='8000',priority='')
        else:
            agi.verbose('OUTSIDE SALES WORKING HOURS....')
            agi.stream_file(excitel_sales_hours)
            agi.hangup()

    elif user_selection == '3':
        agi.verbose('Valid User Input Menu %s' % user_selection)
        CheckCallerStatus(CallerDNID,tenant)
    else:
        agi.verbose('Unknow User Selection Hanging up the call')
        agi.hangup()

def gotoNewUserMenu(PhNumC,tenant):
    inv_retry_count = 2
    no_retry_count = 2
    esc_retry_count = 2

    valid_digits = ['1','2','3']
    invalid_digits = ['4','5','6','7','8','9','0']
    esc_digits = ['*','#']
    no_digits = ['']

    inv_retry_count = 2
    no_retry_count = 2
    esc_retry_count = 2

    while inv_retry_count > 0 or no_retry_count > 0:
        logging.warning('Streaming file for new user selection menu')
        user_input = agi.get_data(excitel_de_new_user_menu_de,DEFAULT_TIMEOUT,max_digits=5)
        agi.verbose('User Entered %s' % user_input)
        logging.warning('User Selected %s' % user_input)

        if user_input in valid_digits:
            agi.verbose('New User Selection Menu - Valid Digits Received %s' % user_input)
            logging.warning('New User Selection Menu - Valid Digits Received %s' % user_input)
            gotoVal = gotoNewUserSelectionMenu(user_input,tenant)
            break
        elif user_input in invalid_digits:
            agi.verbose(' New User Selection Menu - Invalid digits received %s' % user_input)
            logging.warning('New User Selection Menu - Invalid Digits Received %s' % user_input)
            inv_retry_count -= 1
            no_retry_count -=1
            logging.warning('New User Selection Menu -Streaming file for invalid selection')
            agi.stream_file(excitel_invalid_input)
        elif user_input in no_digits:
            agi.verbose('New User Selection Menu - No digits received %s' % user_input)
            logging.warning('No Digits Received %s' % user_input)
            no_retry_count -= 1
            inv_retry_count -= 1
            logging.warning('New User Selection Menu - Streaming file for no selection')
            agi.stream_file(no_selection)
        elif user_input in esc_digits:
            agi.verbose('New User Selection Menu -  No digits received %s' % user_input)
            logging.warning('New User Selection Menu - No Digits Received %s' % user_input)
            esc_retry_count -= 1
            no_retry_count -= 1
            inv_retry_count -= 1
            logging.warning('New User Selection Menu - Streaming file for invalid selection')
            agi.stream_file(excitel_invalid_input)
        else:
            agi.verbose('Unknown User Selection Hanging up the call')
            agi.hangup()

def gotoPartnerMenu(): 
    logging.warning('Going to partners Queues')
    agi.verbose('Going to partners Queues')
    getPartnerWorkHrs = excitelPartnerworkinghours()
    agi.verbose('Partner Working Hours is: %s'%getPartnerWorkHrs)
    if getPartnerWorkHrs == 'Working Hours':
        queue_agents=get_queue_agents('Hyd_partner')
        agi.verbose('return  queue_agents%s' % queue_agents)
        logging.warning('return  queue_agents%s' % queue_agents)
        if queue_agents:
            agi.goto_on_exit(context='excitel_hyd_partner',extension='s',priority='1')
        else:
            agi.goto_on_exit(context='from-internal',extension='8002',priority='')
    elif getPartnerWorkHrs == 'Non Working Hours':
        agi.verbose('Non Working Hours detected')
        agi.stream_file(excitel_nodal_hours)
        agi.hangup()
    else:
        agi.verbose('Unknown Hours')
        agi.hangup()


def gotoCommonQueueMenu():
    logging.warning('Starting Common Queue Menu ')
    agi.verbose('Starting Common Queue Menu')

    cmenu_val_digit = ['1']
    cmenu_inv_digit = ['2','3','4','5','6','7','8','9','0']
    cmenu_no_digit = ['']

    cmenu_inv_cnt = 2
    cmenu_no_cnt = 2
    
    while cmenu_inv_cnt > 0 and cmenu_no_cnt > 0:
        logging.warning('Streaming Common Queue Selection Menu')

        cext_usr_input = agi.get_data(press1,DEFAULT_TIMEOUT,max_digits=1)
        agi.verbose('Userr Selected: %s' % cext_usr_input)
        logging.warning('User Selected %s' % cext_usr_input)

        if cext_usr_input in cmenu_val_digit:
            agi.verbose('Valid Digits Received %s' % cext_usr_input)
            logging.warning('Valid Digits received from exisiting customer %s' % cext_usr_input)
            CommonQMenu(cext_usr_input)
            break
        elif cext_usr_input in cmenu_inv_digit:
            agi.verbose('Invalid Digits Received %s' % cext_usr_input)
            logging.warning('Invalid Digits received from exisiting customer %s' % cext_usr_input)
            logging.warning('Streaming file for invalid selection')
            agi.stream_file(excitel_invalid_input)
            cmenu_inv_cnt -= 1
        elif cext_usr_input in cmenu_no_digit:
            agi.verbose('No Digits Received %s' % cext_usr_input)
            logging.warning('No Digits received from exisiting customer %s' % cext_usr_input)
            logging.warning('Streaming file for no selection')
            agi.stream_file(no_selection)
            cmenu_no_cnt -= 1
        else:
            agi.verbose('Unknown User Selection Hanging up the call')
            agi.hangup()

def CommonQMenu(cqmenu_usr_input):
    logging.warning('Common Queue Menu - User Selection %s' % cqmenu_usr_input)
    agi.verbose('Common Queue Menu - User Selection %s' % cqmenu_usr_input)
    if cqmenu_usr_input == '1':
        agi.verbose('Goto Queue 1 %s' % cqmenu_usr_input)
        queue_agents=get_queue_agents('Hyd_noconnectivity')
        agi.verbose('return  queue_agents%s' % queue_agents)
        logging.warning('return  queue_agents%s' % queue_agents)
        if queue_agents:
            #agi.goto_on_exit(context='excitel_support',extension='s',priority='')
            agi.goto_on_exit(context='excitel_hyd_noconnectivity',extension='s',priority='1')
        else:
            agi.goto_on_exit(context='from-internal',extension='8001',priority='')
    else:
        agi.verbose('Invalid/Unknown Selection Hanging up')
        logging.warning('Invalid/Unknown Selection Hanging up')
        agi.hangup()



def gotoRegisteredUserMenu1(PhoneNum0):
    logging.warning('Starting Existing User Menu for Caller %s' %PhoneNum0)
    agi.verbose('Jumping to Existing User Menu for caller %s'%PhoneNum0)
    logging.warning('Exisintg Customer Mobile Number %s' % PhoneNum0)
    agi.verbose('Phone Number is %s' % PhoneNum0)

    emenu_val_digit = ['1','2','3']
    emenu_inv_digit = ['4','5','6','7','8','9','0']
    emenu_no_digit = ['']

    emenu_inv_cnt = 2
    emenu_no_cnt = 2

    while emenu_inv_cnt > 0 and emenu_no_cnt > 0:
        logging.warning('Streaming Existing User Selection Menu')
        ext_usr_input = agi.get_data(excitel_de_exst_user_menu_de,DEFAULT_TIMEOUT,max_digits=1)
        agi.verbose('Existing Customer Selected: %s' % ext_usr_input)
        logging.warning('Customer Selected %s' % ext_usr_input)

        if ext_usr_input in emenu_val_digit:
            agi.verbose('Valid Digits Received %s' % ext_usr_input)
            logging.warning('Valid Digits received from exisiting customer %s' % ext_usr_input)
            existingUserM1(ext_usr_input)
            break
        elif ext_usr_input in emenu_inv_digit:
            agi.verbose('Invalid Digits Received %s' % ext_usr_input)
            logging.warning('Invalid Digits received from exisiting customer %s' % ext_usr_input)
            logging.warning('Streaming file for invalid selection')
            agi.stream_file(excitel_invalid_input)
            emenu_inv_cnt -= 1
        elif ext_usr_input in emenu_no_digit:
            agi.verbose('No Digits Received %s' % ext_usr_input)
            logging.warning('No Digits received from exisiting customer %s' % ext_usr_input)
            logging.warning('Streaming file for no selection')
            agi.stream_file(no_selection)
            emenu_no_cnt -= 1
        else:
            agi.verbose('Unknown User Selection Hanging up the call')
            agi.hangup()

def existingUserM1(usr_input):
    logging.warning('Existing Customer post selection Menu')
    agi.verbose('Exisitng User Selection Menu %s' % usr_input)
    if usr_input == '1':
        agi.verbose('Goto Queue 1 %s' % usr_input)
        queue_agents=get_queue_agents('Hyd_noconnectivity')
        agi.verbose('return  queue_agents %s' % queue_agents)
        logging.warning('return  queue_agents %s' % queue_agents)
        if queue_agents:
            #agi.goto_on_exit(context='excitel_support',extension='s',priority='')
            agi.goto_on_exit(context='excitel_hyd_noconnectivity',extension='s',priority='1')
        else:
            #agi.goto_on_exit(context='from-internal',extension='8001',priority='')
            agi.goto_on_exit(context='from-internal',extension='8004',priority='')
    elif usr_input == '2':
        agi.verbose('Goto Degradation Queue %s' % usr_input)
        logging.warning('Routing Call to Excitel Degradation Queue')
        queue_agents=get_queue_agents('Hyd_degraded')
        agi.verbose('return queue_agents %s' % queue_agents)
        logging.warning('return queue_agents %s' % queue_agents)
        if queue_agents:
            #agi.goto_on_exit(context='excitel_support',extension='s',priority='')
            agi.goto_on_exit(context='excitel_hyd_degraded',extension='s',priority='1')
        else:
            #agi.goto_on_exit(context='from-internal',extension='8001',priority='')
            agi.goto_on_exit(context='from-internal',extension='8005',priority='')
    elif usr_input == '3':
        agi.verbose('Goto Billing Query Queue %s' % usr_input)
        logging.warning('Routing the Call to Excitel Billing Queue')
        queue_agents=get_queue_agents('Hyd_billing')
        agi.verbose('return  queue_agents %s' % queue_agents)
        logging.warning('return  queue_agents for %s' % queue_agents)
        if queue_agents:
            agi.goto_on_exit(context='excitel_hyd_billing',extension='s',priority='1')
        else:
            agi.goto_on_exit(context='from-internal',extension='8006',priority='')
    else:
        agi.verbose('Unknown User selection hanging up the call')
        agi.hangup()

def gotoRegisteredUserMenu(PhoneNum):
    logging.warning('Starting Existing User Menu for Caller %s' %PhoneNum)
    agi.verbose('Jumping to Existing User Menu for caller %s'%PhoneNum)
    logging.warning('Exisintg Customer Mobile Number %s' % PhoneNum)
    agi.verbose('Phone Number is %s' % PhoneNum)

    emenu_val_digit = ['1','2','3']
    emenu_inv_digit = ['4','5','6','7','8','9','0']
    emenu_no_digit = ['']

    emenu_inv_cnt = 2
    emenu_no_cnt = 2

    while emenu_inv_cnt > 0 and emenu_no_cnt > 0:
        logging.warning('Streaming Existing User Selection Menu')
        ext_usr_input = agi.get_data(excitel_de_exst_user_menu_de,DEFAULT_TIMEOUT,max_digits=1)
        agi.verbose('Existing Customer Selected: %s' % ext_usr_input)
        logging.warning('Customer Selected %s' % ext_usr_input)

        if ext_usr_input in emenu_val_digit:
            agi.verbose('Valid Digits Received %s' % ext_usr_input)
            logging.warning('Valid Digits received from exisiting customer %s' % ext_usr_input)
            existingUserM(ext_usr_input)
            break
        elif ext_usr_input in emenu_inv_digit:
            agi.verbose('Invalid Digits Received %s' % ext_usr_input)
            logging.warning('Invalid Digits received from exisiting customer %s' % ext_usr_input)
            logging.warning('Streaming file for invalid selection')
            agi.stream_file(excitel_invalid_input)
            emenu_inv_cnt -= 1
        elif ext_usr_input in emenu_no_digit:
            agi.verbose('No Digits Received %s' % ext_usr_input)
            logging.warning('No Digits received from exisiting customer %s' % ext_usr_input)
            logging.warning('Streaming file for no selection')
            agi.stream_file(no_selection)
            emenu_no_cnt -= 1
        else:
            agi.verbose('Unknown User Selection Hanging up the call')
            agi.hangup()

def existingUserM(usr_input):
    logging.warning('Existing Customer post selection Menu')
    agi.verbose('Exisitng User Selection Menu %s' % usr_input)
    if usr_input == '1':
        agi.verbose('Goto Queue 1 %s' % usr_input)
        queue_agents=get_queue_agents('Hyd_noconnectivity')
        agi.verbose('return  queue_agents %s' % queue_agents)
        logging.warning('return  queue_agents %s' % queue_agents)
        if queue_agents:
            agi.goto_on_exit(context='excitel_hyd_noconnectivity',extension='s',priority='1')
        else:
            #agi.goto_on_exit(agi.goto_on_exit(context='from-internal',extension='8004',priority=''))
            agi.goto_on_exit(context='from-internal',extension='8004',priority='')
    elif usr_input == '2':
        agi.verbose('Goto Degradation Queue %s' % usr_input)
        queue_agents=get_queue_agents('Hyd_degraded')
        agi.verbose('return  queue_agents %s' % queue_agents)
        logging.warning('return  queue_agents %s' % queue_agents)
        logging.warning('Routing Call to Excitel Degraded Queue')
        if queue_agents:
            agi.goto_on_exit(context='excitel_hyd_degraded',extension='s',priority='1')
        else:
            agi.goto_on_exit(context='from-internal',extension='8005',priority='')
    elif usr_input == '3':
        agi.verbose('Goto Billing Query Queue %s' % usr_input)
        queue_agents=get_queue_agents('Hyd_billing')
        agi.verbose('return  queue_agents %s' % queue_agents)
        logging.warning('return  queue_agents %s' % queue_agents)
        logging.warning('Routing the Call to Excitel Billing Queue')
        if queue_agents:
            agi.goto_on_exit(context='excitel_hyd_billing',extension='s',priority='1')
        else:
            agi.goto_on_exit(context='from-internal',extension='8006',priority='')

    else:
        agi.verbose('Unknown User selection hanging up the call')
        agi.hangup()


def gotoLocalTicketMenu(GetCode):
    logging.warning('Into Local Ticket Menu - Code %s' % GetCode)
    agi.verbose('Into Local Ticket Menu - Code %s' % GetCode)
    repeat = repeat_caller_check(phone=CallerDNID)
    agi.verbose('repeat_caller_check %s' % repeat)
    repeat_call_identification = json.loads(repeat.text)
    repeat_call_iden = repeat_call_identification['data']['present']



    # if GetCode in LocalTickets_ErrorCodes:
    #     agi.verbose('belongs to Local ticket for No Connectivity, no ETR or ETR more than 8h %s' % GetCode)
    #     c = S+str(GetCode)

    #     agi.stream_file(S+str(GetCode))

    #     repeat = repeat_caller_check(phone=CallerDNID)
    #     agi.verbose('repeat_caller_check %s' % repeat)

    #     agi.hangup()

    if GetCode == 200:
        agi.verbose('200 - belongs to Local ticket for No Connectivity, no ETR or ETR more than 8h %s' % GetCode)
        agi.stream_file(S200)
        # CallerDetails(CallerDNID)
        if repeat_call_iden:
            gotoCommonQueueMenu()
        else:
            agi.hangup()
    elif GetCode == 201:
        agi.verbose('201 - belongs to Local ticket for No Connectivity, ETR up to 1h %s' % GetCode)
        agi.stream_file(S201)
        if repeat_call_iden:
            gotoCommonQueueMenu()
        else:
            agi.hangup()
    elif GetCode == 202:
        agi.verbose('202 - belongs to Local ticket for No Connectivity, ETR up to 2h %s' % GetCode)
        agi.stream_file(S202)
        if repeat_call_iden:
            gotoCommonQueueMenu()
        else:
            agi.hangup()
    elif GetCode == 203:
        agi.verbose('203 - belongs to Local ticket for No Connectivity, ETR up to 3h %s' % GetCode)
        agi.stream_file(S203)
        if repeat_call_iden:
            gotoCommonQueueMenu()
        else:
            agi.hangup()
    elif GetCode == 204:
        agi.verbose('204 - belongs to Local ticket for No Connectivity, ETR up to 4h %s' % GetCode)
        agi.stream_file(S204)
        if repeat_call_iden:
            gotoCommonQueueMenu()
        else:
            agi.hangup()
    elif GetCode == 205:
        agi.verbose('205 - belongs to Local ticket for No Connectivity, ETR up to 5h %s' % GetCode)
        agi.stream_file(S205)
        if repeat_call_iden:
            gotoCommonQueueMenu()
        else:
            agi.hangup()
    elif GetCode == 206:
        agi.verbose('206 - belongs to Local ticket for No Connectivity, ETR up to 6h %s' % GetCode)
        agi.stream_file(S206)
        if repeat_call_iden:
            gotoCommonQueueMenu()
        else:
            agi.hangup()
    elif GetCode == 207:
        agi.verbose('207 - belongs to Local ticket for No Connectivity, ETR up to 7h %s' % GetCode)
        agi.stream_file(S207)
        if repeat_call_iden:
            gotoCommonQueueMenu()
        else:
            agi.hangup()
    elif GetCode == 208:
        agi.verbose('208 - belongs to Local ticket for No Connectivity, ETR up to 8h %s' % GetCode)
        agi.stream_file(S208)
        if repeat_call_iden:
            gotoCommonQueueMenu()
        else:
            agi.hangup()
    else:
        agi.verbose('2xx - Unknown Code Received, will hangup %s' % StatusCode)
        agi.hangup()



def gotoLocalTicketOth(GetCode2):
    logging.warning('Into Local Ticket Others Menu - Code %s' % GetCode2)
    agi.verbose('Into Local Ticket Menu - Code %s' % GetCode2)

    if GetCode2 == 210:
        agi.verbose('210 - belongs to Local ticket for No Connectivity, ETR more than  8h %s' % GetCode2)
        agi.stream_file(S210)
        gotoCommonQueueMenu()
    elif GetCode2 == 211:
        agi.verbose('belongs to Local ticket for any other reason, ETR up to 1h %s' % GetCode2)
        agi.stream_file(S211)
        gotoCommonQueueMenu()
    elif GetCode2 == 212:
        agi.verbose('belongs to Local ticket for any other reason, ETR up to 2h %s' % GetCode2)
        agi.stream_file(S212)
        gotoCommonQueueMenu()
    elif GetCode2 == 213:
        agi.verbose('belongs to Local ticket for any other reason, ETR up to 3h %s' % GetCode2)
        agi.stream_file(S213)
        gotoCommonQueueMenu()
    elif GetCode2 == 214:
        agi.verbose('belongs to Local ticket for any other reason, ETR up to 4h %s' % GetCode2)
        agi.stream_file(S214)
        gotoCommonQueueMenu()
    elif GetCode2 == 215:
        agi.verbose('belongs to Local ticket for any other reason, ETR up to 5h %s' % GetCode2)
        agi.stream_file(S215)
        gotoCommonQueueMenu()
    elif GetCode2 == 216:
        agi.verbose('belongs to Local ticket for any other reason, ETR up to 6h %s' % GetCode2)
        agi.stream_file(S216)
        gotoCommonQueueMenu()
    elif GetCode2 == 217:
        agi.verbose('belongs to Local ticket for any other reason, ETR up to 7h %s' % GetCode2)
        agi.stream_file(S217)
        gotoCommonQueueMenu()
    elif GetCode2 == 218:
        agi.verbose('belongs to Local ticket for any other reason, ETR up to 8h %s' % GetCode2)
        agi.stream_file(S218)
        gotoCommonQueueMenu()
    else:
        agi.verbose('2xx - Unknown Code Received, will hangup %s' % GetCode2)
        agi.hangup()

def gotoGlobalTicket(GetCode3):
     logging.warning('Into Global Ticket Menu - Code %s' % GetCode3)
     agi.verbose('Into Global Ticket Menu - Code %s' % GetCode3)

     if GetCode3 == 300:
         agi.verbose('Code 300 - belongs to Global ticket, no ETR or ETR more than 8h %s' % GetCode3)
         agi.stream_file(S300)
         gotoCommonQueueMenu()
     elif GetCode3 == 301:
         agi.verbose('301 - belongs to Global ticket, ETR up to 1h %s' % GetCode3)
         agi.stream_file(S301)
         gotoCommonQueueMenu()
     elif GetCode3 == 302:
         agi.verbose('302 - belongs to Global ticket, ETR up to 2h %s' % GetCode3)
         agi.stream_file(S302)
         gotoCommonQueueMenu()
     elif GetCode3 == 303:
         agi.verbose('303 - belongs to Global ticket, ETR up to 3h %s' % GetCode3)
         agi.stream_file(S304)
         gotoCommonQueueMenu()
     elif GetCode3 == 304:
         agi.verbose('304 - belongs to Global ticket, ETR up to 4h %s' % GetCode3)
         agi.stream_file(S304)
         gotoCommonQueueMenu()
     elif GetCode3 == 305:
         agi.verbose('305 - belongs to Global ticket, ETR up to 5h %s' % GetCode3)
         agi.stream_file(S305)
         gotoCommonQueueMenu()
     elif GetCode3 == 306:
         agi.verbose('306 - belongs to Global ticket, ETR up to 6h %s' % GetCode3)
         agi.stream_file(S306)
         gotoCommonQueueMenu()
     elif GetCode3 == 307:
         agi.verbose('307 - belongs to Global ticket, ETR up to 7h %s' % GetCode3)
         agi.stream_file(S307)
         gotoCommonQueueMenu()
     elif GetCode3 == 308:
         agi.verbose('308 - belongs to Global ticket, ETR up to 8h %s' % GetCode3)
         agi.stream_file(S308)
         gotoCommonQueueMenu()
     else:
         agi.verbose('3xx - Unknown Code Received, will hangup %s' % GetCode3)
         agi.hangup()



if CheckSubscriber in No_User:
    agi.verbose('New User detected - Code :%s ' % CheckSubscriber)
    gotoNewUserMenu(CallerDNID,tenant_name)
elif CheckSubscriber in User_Other:
    agi.verbose('Registered User Deteteced - Code: %s' % CheckSubscriber)
    gotoRegisteredUserMenu(CallerDNID)
elif CheckSubscriber in User_WO_T:
    agi.verbose('Registered User With no Ticket Detected - Code %s' % CheckSubscriber)
    gotoRegisteredUserMenu(CallerDNID)
elif CheckSubscriber in Partner_Response:
    agi.verbose('Partner Detected ')
    gotoPartnerMenu()
elif CheckSubscriber in LocalTickets_ErrorCodes:
    agi.verbose('User Detected with Local Ticket - Code %s' % CheckSubscriber)
    logging.warning('User Detected with Local Ticket - Code %s' % CheckSubscriber)
    localTkt = gotoLocalTicketMenu(CheckSubscriber)
elif CheckSubscriber in LocalTicketsOth_ErrorCode:
    agi.verbose('User Detected with Local Ticket Others - Code %s' % CheckSubscriber)
    logging.warning('User Detected with Local Ticket Others - Code %s' % CheckSubscriber)
    localTktOth = gotoLocalTicketOth(CheckSubscriber)
elif CheckSubscriber in GlobalTickets_ErrorCode:
    agi.verbose('User Detected With Global ticket - Code %s' % CheckSubscriber)
    globalTkt = gotoGlobalTicket(CheckSubscriber)
else:
    agi.verbose('Unknown Code detected hanging up the call - Code %s' % CheckSubscriber)
    agi.hangup()

    



