import requests
import json
def repeat_caller_check(phone,url = "http://192.168.202.12:5005/livechat_backend/get_repeat_ticket_call"):

    payload = json.dumps({
      "phone_no": phone
    })
    headers = {
      'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    return response


if __name__ == "__main__":
    repeat_caller_check(phone="9920482274")