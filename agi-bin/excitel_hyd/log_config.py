#!/usr/bin/python3

import logging

logging.basicConfig(filename='/var/log/excitel_ivr.log', filemode='a' ,format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S')
