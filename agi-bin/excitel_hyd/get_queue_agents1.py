#!/usr/bin/python3

import os
import sys
import logging
import log_config
import json
from asterisk.agi import *
from dotenv import load_dotenv
dotenv_path = os.path.join("/var/lib/asterisk/agi-bin/excitel_hyd", ".env")
load_dotenv(dotenv_path, verbose=True)
ami_host = os.getenv("AMI_HOST")

queue_name = sys.argv[1]
ext = sys.argv[2]
que_context = sys.argv[3]
que_position = sys.argv[4]

agi = AGI()
agi.verbose("Entering into Python AGI...")
agi.answer()

def get_queue_agents(queue):
    import requests
    import json
    url = "http://192.168.203.12:5005/livechat_backend/get_queue_agents"
    payload = json.dumps({
    "ami_host": ami_host,
    "queue": queue
    })
    headers = {
    'Content-Type': 'application/json'
    }
    response = requests.request("POST", url, headers=headers, data=payload)
    if response.status_code==200:
        ag_status = response.json()['data']['agent_present']
        agi.verbose("AGENT STATUS %s" % ag_status)
        return response.json()['data']['agent_present']
        logging.warning('get queue agents executed')


agent_status = get_queue_agents(queue_name)

if agent_status==True:
	#agi.goto_on_exit(context='from-worker2',extension='8012',priority='')
        #agi.goto_on_exit(context='excitel_sales',extension='s',priority='')
	agi.set_variable('__requeue','1')
	agi.goto_on_exit(context=que_context,extension='s',priority='')
elif agent_status=='empty':
        agi.goto_on_exit(context='no-agents-available',extension='s',priority='1')
else:
	#agi.goto_on_exit(context='from-worker2',extension='8012',priority='')
        agi.goto_on_exit(context='from-internal',extension=ext,priority='')
