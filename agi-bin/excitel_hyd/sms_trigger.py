#!/usr/bin/python3

import urllib.request
import urllib.parse


def SMS_response(api_key, numbers, sender, message):
    data =  urllib.parse.urlencode({'api_key': api_key, 'numbers': numbers,'message' : message, 'sender': sender})
    data = data.encode('utf-8')
    request = urllib.request.Request("https://api.textlocal.in/send/?")  #tried this changing to http as well as per their suggestion
    f = urllib.request.urlopen(request, data)
    fr = f.read()
    return(fr)

resp =  SMS_response('NWVlMWQ0NjU4YWRmYmMxYTRhMWE3NzYxYTIxNGI5Y2Y', '9702243283','EXCITL', 'We are experiencing higher wait time at our Call Center, alternatively, you may raise a Ticket or Chat with us on MyExcitel App http://onelink.to/qbw6t4%nExcitel')
print (resp)
