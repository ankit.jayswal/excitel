import soundfile,os

def convertAllFilesInDirectoryTo16Bit(directory):
    for file in os.listdir(directory):
        if(file.endswith('.wav')):
            nameSolo = file.rsplit('.', 1)[0]
            print(directory + nameSolo )
            from pydub import AudioSegment
            sound = AudioSegment.from_wav(directory + file)
            sound = sound.set_channels(1)
            sound.export(directory+"dev/" + nameSolo + '.wav', format="wav")
            print("converting " + file + "to 16 - bit")


convertAllFilesInDirectoryTo16Bit("/home/admin-209/Documents/excitel/Downtime Annoucements/dev/")